<div class="box">

<div class="left">

<img src="img/portrait.jpg" alt="Jeric de Leon">

</div>

<div class="right">

# Jeric de Leon

<p><div class="box"><i class="fas fa-key"></i><span>&nbsp;</span><a href="https://keyoxide.org/54E3A7537100F7C45E35CB9B306A2FF94D024D5E"><pre><sup>54E3 A753 7100 F7C4 5E35</sup><br/><sup>CB9B 306A 2FF9 4D02 4D5E</sup></pre></a></div></p>

<i class="fas fa-envelope"></i> &nbsp; [mail@jericdeleon.com](mailto:mail@jericdeleon.com)

<i class="fab fa-gitlab"></i> &nbsp; [\@jericdeleon](https://gitlab.com/jericdeleon)

<i class="fab fa-twitter"></i> &nbsp; [\@jericpauldeleon](https://twitter.com/jericpauldeleon)

<i class="fas fa-briefcase"></i> &nbsp; [My Resume](https://jericdeleon.gitlab.io/resume)

<i class="fas fa-tools"></i> &nbsp; [Tools I Use](https://jericdeleon.gitlab.io/tools.html)

</div>

</div>
