# Tools I Use

## Software

**Operating System**
: I'm currently on **openSUSE Tumbleweed**, as its sysadmin-centric orientation fits really well with what I expect from a proper OS. I wanted Arch's rolling release model, but with a pathway to enterprise, and SUSE's ecosystem won me over RedHat's. For the random non-Linux tool, I keep a handy separate Windows 10 drive / partition.

**Automation**
: I frequently nuke my desktop / laptop installations, to enforce myself to treat them as cattle rather than pets. To that end, I use **Ansible** to bootstrap not just my desktop / laptops , but even my **libvirt-based** **Vagrant** VMs as well. You can find my [playbooks here](https://gitlab.com/jericdeleon/bootstrap-host).

**Terminal**
: I use the terminal for everything. My current toolset consists of: **alacritty**, **tmux**, **zsh** and **asdf**, with **lf** and **lazydocker** as some of my go-to terminal UIs. I manage my config files with **chezmoi**, and you can find my [dotfiles here](https://gitlab.com/jericdeleon/dotfiles).

**Editor**
: **vim** is the editor that keeps on giving, and I have yet to reach the ceiling of both my skill in it, and of the things that it allows me to do. Find my [.vimrc here](https://gitlab.com/jericdeleon/dotfiles/-/blob/master/dot_vimrc).

**Notes**
: **vimwiki** has revolutionized my notetaking by making it as simple as possible, and the HTML output is the cherry on top. **pandoc** lets me use a single markdown input file and convert it to HTML, PDF and DOCX, which I have used in small sites and in my [resume here](https://jericdeleon.gitlab.io/resume/) to great success ([source here](https://gitlab.com/jericdeleon/resume)).

**Accounting**
: I've been very satisfied with **hledger**, and it has allowed me track finances sanely: it's file-based (so I can put it on version control), has a terminal UI, a web UI, proper budgeting / forecasting and multiple commodity support. I will happily recommend it to anyone comfortable in the terminal.

## Hardware

**Security**
: I use a couple of **Yubikey NFC**s for hardware-based U2F / OATH. They also contain my **pgp** subkeys for git, ssh, and encryption, which I heavily rely on. You can find my public key in my [keyoxide profile here](https://keyoxide.org/54E3A7537100F7C45E35CB9B306A2FF94D024D5E).

**Keyboard**
: I built a low-profile **Corne keyboard**, which is a [very portable and highly customizable split ergonomic keyboard](https://github.com/foostan/crkbd). QMK's layer keys allow me to access numpad keys, system function keys (volume, brightness), and mouse control keys without even moving my wrist: coding-induced wrist and shoulder pain has never plagued me since. You can find my [keymaps here](https://gitlab.com/jericdeleon/qmk-firmware/-/tree/dc94d73c6c22545c69798177601cdb7fe0fed192/keyboards/crkbd/keymaps/jericdeleon).

**Mouse**
: The **Elecom Deft Pro** is my first trackball mouse, and while I have yet to try others, I'm already content. This, together with my **Corne keyboard**, has allowed me to minimize wrist movement and get rid of RSI symptoms altogether.

**PC Stick**
: I always carry a PC stick with me, which right now is an **Intel Compute Stick**, just in case an emergency arises and I need a system to work in. Fits in my **Corne keyboard** case.

**Laptop**
: Currently rocking a **Thinkpad T450**. For the last decade, whenever I find myself in need of a laptop, I've stabilized with: getting the best-priced 4-year-old Thinkpad in the secondhand market, and upgrading / repairing it myself (4 years usually mark the end of corporate contracts, and so a lot of companies offload their used laptops in the secondhand market at this point, and the competition drives the price down. This plan ends up with me only spending 200-300 USD for a competitive machine). Thinkpads are sturdy, upgradeable, and most of all, well-supported by Linux drivers, and thus my top choice unless dethroned.

**Desktop**
: My main machine is a **Ryzen 1600 AF** with **RX 580** in a **SilverStone Sugo SG13**, which fits inside my carry-on whenever I travel somewhere for more than a month.
