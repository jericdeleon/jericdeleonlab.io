# jericdeleon.gitlab.io
Generates html from a single markdown file (using pandoc).

## Usage
1. Install dependencies:
    - `pandoc`
2. Build:
    ```
    ./build
    ```
